This is a repository only for testing programs and changes before pushing them to their upstream sources, or keeping them here in case they cannot be added to Alpine Linux repositories due to their eldness.

- Picked from https://github.com/DDoSolitary/alpine-repo/blob/master/README.md

# How to use

1. Trust my public key.

```
wget -P /etc/apk/keys https://aat-linux-repository.sourceforge.io/aaloyt@users.sourceforge.net-5fe2133b.rsa.pub
```

2. I only build packages for the edge releases, so [upgrade to it](https://wiki.alpinelinux.org/wiki/Upgrading_Alpine#Upgrading_to_Edge) if you're using stable releases.

3. Add this line to `/etc/apk/repositories`

```
https://aat-linux-repository.sourceforge.io/alpine/edge/main
https://aat-linux-repository.sourceforge.io/alpine/edge/community
https://aat-linux-repository.sourceforge.io/alpine/edge/testing
```

4. Update your local index.

```
apk update
```

5. **Enjoy it!**
