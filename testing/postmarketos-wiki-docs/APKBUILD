# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=postmarketos-wiki-docs
pkgver=0_git20220615
pkgrel=0
_commit="f1a3c45c815ea041037f5831c6556479628006b3"
pkgdesc="Pages from PostmarketOS Wiki optimized for offline browsing"
url="https://gitlab.com/antoni.aloytorrens/postmarketos-wiki-docs"
arch="noarch"
license="MIT"
makedepends="python3 py3-simplemediawiki py3-lxml py3-cssselect py3-requests py3-beautifulsoup4"
source="
	$pkgname-$_commit.tar.gz::https://gitlab.com/antoni.aloytorrens/postmarketos-wiki-docs/-/archive/$_commit.tar.gz
	org.postmarketos.Docs.png
	postmarketos-wiki-docs.desktop
	"
options="!check" # No test suite
builddir="$srcdir/$pkgname-$_commit"

build() {
	LANG=en_US.UTF-8 python3 \
	postmarketos-wiki-docs.py \
	--output-directory "$srcdir/build_wiki" \
	--clean \
	--safe-filenames

	LANG=en_US.UTF-8 python3 \
	add-missing-styles.py \
	--directory "$srcdir/build_wiki/en"
}

package() {
	install -vdm755 "$pkgdir/usr/share/doc/postmarketos-wiki/html"
	cp -r "$srcdir"/build_wiki/* "$pkgdir/usr/share/doc/postmarketos-wiki/html"

	install -Dm644 "$srcdir"/postmarketos-wiki-docs.desktop \
	"$pkgdir"/usr/share/applications/postmarketos-wiki-docs.desktop

	install -Dm644 "$srcdir"/org.postmarketos.Docs.png \
	"$pkgdir"/usr/share/icons/org.postmarketos.Docs.png
}

sha512sums="
84652921995bb03ae7626bd233de47ea197132908bd8d9b18c7cb7aadaeba22ad6b4d45f0ec4555b8276dac4ada06e51a84e2a45c77bc8dd2606593927f53dc0  postmarketos-wiki-docs-f1a3c45c815ea041037f5831c6556479628006b3.tar.gz
273f0c9b0a5d02d83e4589942f413651c064cf6a834379d28bb5f61d53f7897f34270d58944e93bf2a5601b73549978725e82a0b2c33e12100cc9235d284169f  org.postmarketos.Docs.png
d6c7a8725d4ac3bbb455b655defbdc38f4518bdbbc0fe09f5407dcd8c6537defff79511e9ff2bd23cc11431013b45c0f68f330a1263517027bd6a3c5dfea112d  postmarketos-wiki-docs.desktop
"
